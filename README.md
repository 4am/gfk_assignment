# Average Ratings Calculation based on given condition.

Condition: Calculate Average Ratings per Genre and Year Released. Given that Year Released is from 1989 and User are of age 18 to 49.

## Repository Structure:
- _ml-1m_ contains data *.dat files.
- _src_ contains source and test code.
- _result_ contains results : Average Rating Per Genre and Year as follows:
  
  **((Genre, Year Released), Average)**  e.g. ((Action,1992), 3.1441974757064672)

## Approach: 
- Parse data file in to JavaRDD.
- Filter JavaRDD based on condition.
- Join JavaRDD based on condition.
- Group JavaRDD based on condition.
- Reduce to Average.

## Languages and Frameworks:
- Openjdk 11.0.7
- Spark-3.1.2-bin-hadoop3.2
- Junit 5.7.0

## Future work:
- Try using DataFrame or DataSet instead of JavaRDD. 
- Improve Coding style
- Improve writing unittest for Java
