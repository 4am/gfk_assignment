package gfk_assignment;

import java.util.List;
import java.util.Arrays;
import scala.Tuple2;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.SparkConf;

public class AvrRatingsCalculation {
    public static String path="ml-1m";

    static Function<User, Boolean> filterAge18To49 = user -> (
            user.getAgeGroup() <= 45 && user.getAgeGroup() >= 18
    );

    static Function<Movie, Boolean> filterMoviesFrom1989 = movie -> (movie.getYearReleased() >= 1989);

    static FlatMapFunction<Movie, Movie> unpivotMoviesByGenre =  movie -> {
        List<String> genres = Arrays.asList(movie.getGenre().split("\\|"));
        Movie[] unpivotLine = genres.stream()
                .map(genre -> new Movie(movie.getMovieId(), movie.getTitle(), movie.getYearReleased(), genre))
                .toArray(Movie[]::new);
        return Arrays.asList(unpivotLine).iterator();
    };

    static PairFunction<Tuple2<Integer, Tuple2<Tuple2<Rating, Movie>, User>>,
        Tuple2<String, Integer>, Tuple2<Integer, Integer>>
        groupRatingByGenreAndYear = row -> {
        Tuple2<Rating, Movie> ratingNMovie = row._2()._1;
        Rating rating = ratingNMovie._1;
        Movie movie = ratingNMovie._2;
        Tuple2<String, Integer> genreAndYear = new Tuple2<>(movie.getGenre(), movie.getYearReleased());
        Tuple2<Integer, Integer> ratingAndCounter = new Tuple2<>(rating.getRating(), 1);
        return new Tuple2<>(genreAndYear, ratingAndCounter);
    };

    public static void main(String[] args) {
        SparkConf conf = new SparkConf().setAppName("avrRating").setMaster("local");
        JavaSparkContext jsc = new JavaSparkContext(conf);
        JavaRDD<Rating> ratingsRDD = jsc.textFile(path+"/ratings.dat")
                .map(Rating::parseRating);

        JavaRDD<User> usersRDD = jsc.textFile(path+"/users.dat")
                .map(User::parseUser)
                .filter(filterAge18To49);

        JavaRDD<Movie> moviesRDD = jsc.textFile(path+"/movies.dat")
                .map(Movie::parseMovie)
                .filter(filterMoviesFrom1989)
                .flatMap(unpivotMoviesByGenre);

        JavaPairRDD<Integer, Rating> pairRatingRDD = ratingsRDD
                .mapToPair(rating -> new Tuple2<>(rating.getMovieId(), rating));
        JavaPairRDD<Integer, Movie> pairMovieRDD = moviesRDD
                .mapToPair(movie -> new Tuple2<>(movie.getMovieId(), movie));

        JavaPairRDD<Integer, Tuple2<Rating, Movie>> joinRatingMovieRDD = pairRatingRDD.join(pairMovieRDD);

        JavaPairRDD<Integer, Tuple2<Rating, Movie>> pairJoinRatingMovieRDD = joinRatingMovieRDD
                .map(ratingMovieRow -> ratingMovieRow._2)
                .mapToPair(ratingMovie -> new Tuple2<>(ratingMovie._1.getUserId(), ratingMovie));
        JavaPairRDD<Integer, User> pairUserRDD = usersRDD
                .mapToPair(user -> new Tuple2<>(user.getUserId(), user));

        JavaPairRDD<Integer, Tuple2<Tuple2<Rating, Movie>, User>> joinRatingMovieUserRDD = pairJoinRatingMovieRDD
                .join(pairUserRDD);

        JavaPairRDD<Tuple2<String, Integer>, Tuple2<Integer, Integer>> ratingPerGenreAndYearRDD =
                joinRatingMovieUserRDD.mapToPair(groupRatingByGenreAndYear);

        JavaPairRDD<Tuple2<String, Integer>, Tuple2<Integer, Integer>> sumRatingPerGenreAndYearRDD =
                ratingPerGenreAndYearRDD.reduceByKey((v1, v2) -> new Tuple2<>(v1._1 + v2._1, v1._2 + v2._2));

        JavaPairRDD<Tuple2<String, Integer>, Double> avrRatingPerGenreAndYearRDD = sumRatingPerGenreAndYearRDD
                .mapToPair(row -> new Tuple2<>(row._1, ((double)row._2()._1/(double)row._2()._2)));

        avrRatingPerGenreAndYearRDD.saveAsTextFile("result");
    }
}
