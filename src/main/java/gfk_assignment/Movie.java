package gfk_assignment;

import java.io.Serializable;
import java.lang.Integer;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Movie implements Serializable{
    private final int movieId;
    private final String title;
    private final int yearReleased;
    private final String genre;

    public Movie(int movieId, String title, int yearReleased, String genre) {
        this.movieId = movieId;
        this.title = title;
        this.yearReleased = yearReleased;
        this.genre = genre;
    }

    public int getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public int getYearReleased() {
        return yearReleased;
    }

    public String getGenre() {
        return genre;
    }

    public static Movie parseMovie(String str) {
        String[] fields = str.split("::");
        if (fields.length != 3) {
            throw new IllegalArgumentException("Each line in MoviesData must contain 3 fields");
        }
        int movieId = Integer.parseInt(fields[0]);
        String genre = fields[2];
        String title = fields[1];
        String yearReleasePattern = "\\w.*\\((\\d*)\\)";
        Matcher matcher = Pattern.compile(yearReleasePattern).matcher(title);
        int yearReleased = matcher.find() ? Integer.parseInt(matcher.group(1)) : 0;
        return new Movie(movieId, title, yearReleased, genre);
    }

    @Override
    public String toString() {
        return ("MoviesID: " + this.getMovieId()+
                " Tittle: " + this.getTitle() +
                " YearReleased: " + this.getYearReleased() +
                " Genre : " + this.getGenre());
    }
}
