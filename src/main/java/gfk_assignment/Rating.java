package gfk_assignment;

import java.io.Serializable;
import java.lang.Integer;

public class Rating implements Serializable {
    private final int userId;
    private final int movieId;
    private final int rating;

    public Rating(int userId, int movieId, int rating) {
        this.userId = userId;
        this.movieId = movieId;
        this.rating = rating;
    }

    public int getUserId() {
        return userId;
    }

    public int getMovieId() {
        return movieId;
    }

    public int getRating() {
        return rating;
    }

    public static Rating parseRating(String str) {
        String[] fields = str.split("::");
        if (fields.length != 4) {
            throw new IllegalArgumentException("Each line in RatingsData must contain 4 fields");
        }
        int userId = Integer.parseInt(fields[0]);
        int movieId = Integer.parseInt(fields[1]);
        int rating = Integer.parseInt(fields[2]);
        return new Rating(userId, movieId, rating);
    }

    @Override
    public String toString() {
        return ("MoviesID: " + this.getMovieId()+
                " UserID: " + this.getUserId() +
                " Rating: " + this.getRating());
    }
}
