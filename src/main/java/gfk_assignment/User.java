package gfk_assignment;

import java.io.Serializable;
import java.lang.Integer;

public class User implements Serializable {
    private final int userId;
    private final int ageGroup;

    public User(int userId, int ageGroup) {
        this.userId = userId;
        this.ageGroup = ageGroup;
    }

    public int getUserId() {
        return userId;
    }

    public int getAgeGroup() {
        return ageGroup;
    }

    public static User parseUser(String str){
        String[] fields = str.split("::");
        if (fields.length != 5) {
            throw new IllegalArgumentException("Each line in UserData must contain 5 fields");
        }
        int userId = Integer.parseInt(fields[0]);
        int ageGroup = Integer.parseInt(fields[2]);
        return new User(userId, ageGroup);
    }

    @Override
    public String toString() {
        return ("UserID: " + this.getUserId() +
                " AgeGroup: " + this.getAgeGroup());
    }
}
