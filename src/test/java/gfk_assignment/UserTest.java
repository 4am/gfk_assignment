package gfk_assignment;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class UserTest {
    private User user;
    private String userRawText;

    @Test
    @DisplayName("User instant should be constructed properly")
    public void testUserConstructor() {
        user = new User(2,56);

        Assertions.assertEquals(user.getUserId(), 2);
        Assertions.assertEquals(user.getAgeGroup(), 56);
    }

    @Test
    @DisplayName("Text should be parsed into user instant properly")
    public void testParseRating() {
        userRawText = "12::M::25::12::32793"; // UserID::Gender::Age::Occupation::Zip-code

        user = User.parseUser(userRawText);

        Assertions.assertEquals(user.getUserId(), 12);
        Assertions.assertEquals(user.getAgeGroup(), 25);
    }

    @Test
    @DisplayName("Parser should throw exception for invalid user text")
    public void testParseUserException() {
        userRawText = "12::M::25::12::32793::123:123";

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            User.parseUser(userRawText);
        });
        Assertions.assertTrue(exception.getMessage().contains("must contain 5 fields"));
    }
}
