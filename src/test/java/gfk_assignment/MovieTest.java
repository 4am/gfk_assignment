package gfk_assignment;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

class MovieTest {
    private Movie movie;
    private String movieRawText;

    @Test
    @DisplayName("Movie instant should be constructed properly")
    public void testMovieConstructor() {
        movie = new Movie(14, "Nixon (1995)", 1995, "Drama");

        Assertions.assertEquals(movie.getMovieId(), 14);
        Assertions.assertEquals(movie.getGenre(), "Drama");
        Assertions.assertEquals(movie.getTitle(), "Nixon (1995)");
        Assertions.assertEquals(movie.getYearReleased(), 1995);
    }

    @Test
    @DisplayName("Text should be parsed into movie instant properly")
    public void testParseMovie() {
        movieRawText = "1::Toy Story (1995)::Animation|Children's|Comedy"; //MovieID::Title::Genres

        movie = Movie.parseMovie(movieRawText);

        Assertions.assertEquals(movie.getMovieId(), 1);
        Assertions.assertEquals(movie.getGenre(), "Animation|Children's|Comedy");
        Assertions.assertEquals(movie.getTitle(), "Toy Story (1995)");
        Assertions.assertEquals(movie.getYearReleased(), 1995);
    }

    @Test
    @DisplayName("Parser should throw exception for invalid movie text")
    public void testParseMovieException() {
        movieRawText = "1::Animation|Children's|Comedy";

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Movie.parseMovie(movieRawText);
        });
        Assertions.assertTrue(exception.getMessage().contains("must contain 3 fields"));
    }
}
