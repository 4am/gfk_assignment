package gfk_assignment;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class RatingTest {
    private Rating rating;
    private String ratingRawText;

    @Test
    @DisplayName("Rating instant should be constructed properly")
    public void testRatingConstructor() {
        rating = new Rating(1, 1193, 5);

        Assertions.assertEquals(rating.getMovieId(), 1193);
        Assertions.assertEquals(rating.getUserId(), 1);
        Assertions.assertEquals(rating.getRating(), 5);
    }

    @Test
    @DisplayName("Text should be parsed into rating instant properly")
    public void testParseRating() {
        ratingRawText = "1::919::4::978301368"; // UserID::MovieID::Rating::Timestamp

        rating = Rating.parseRating(ratingRawText);

        Assertions.assertEquals(rating.getMovieId(), 919);
        Assertions.assertEquals(rating.getUserId(), 1);
        Assertions.assertEquals(rating.getRating(), 4);
    }

    @Test
    @DisplayName("Parser should throw exception for invalid rating text")
    public void testParseRatingException() {
        ratingRawText = "1::919::4";

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Rating.parseRating(ratingRawText);
        });
        Assertions.assertTrue(exception.getMessage().contains("must contain 4 fields"));
    }
}
